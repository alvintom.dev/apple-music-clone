import 'package:dio/dio.dart';

abstract class HttpState<T> {
  final T? data;
  final DioException? exception;

  const HttpState({this.data, this.exception});
}

class HttpSuccess<T> extends HttpState<T> {
  const HttpSuccess(T data) : super(data: data);
}

class HttpFailed<T> extends HttpState<T> {
  const HttpFailed(DioException exception) : super(exception: exception);
}
