import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:apple_music_clone/core/constants/api_constants.dart';
import 'package:dio/dio.dart';

class TokenApiService {
  Future<Response<Map<String, dynamic>>> getToken(
      String clientSecret) async {

    //use http instead of DIO,
    // currently DIO have some issue with handling x-www-form-urlencoded type
    final res = await http.post(
      Uri.parse(musicApiTokenUrl),
      headers: {
        HttpHeaders.authorizationHeader: 'Basic $clientSecret',
        HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded',
      },
      encoding: Encoding.getByName('utf-8'),
      body: {"grant_type": "client_credentials"},
    );


    return Response(
        data: jsonDecode(res.body),
        requestOptions: RequestOptions(),
        statusCode: res.statusCode);
  }
}
