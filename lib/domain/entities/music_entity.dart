class MusicEntity {
  final String id;
  final String artistName;
  final String name;
  final String img;

  const MusicEntity({
    required this.id,
    required this.artistName,
    required this.name,
    required this.img,
  });
}
