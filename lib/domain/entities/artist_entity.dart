class ArtistEntity {
  final String id;
  final String name;
  final int followerCount;
  final String img;

  ArtistEntity({
    required this.id,
    required this.name,
    required this.followerCount,
    required this.img,
  });
}
