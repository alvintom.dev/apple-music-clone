class AlbumEntity {
  final String id;
  final String artistName;
  final String name;
  final String img;

  const AlbumEntity({
    required this.id,
    required this.artistName,
    required this.name,
    required this.img,
  });
}
