class SampleData {
  static List<Map<String, dynamic>> browseItems = [
    {
      'id': 1,
      'browse_type': 'Listen Now',
      'browse_title': 'Malaysian Music Today',
      'browse_subtitle': 'Apple Music Malaysian Music',
      'description':
      "The country's best--catchy rap, energetic, indie and thrilling pop.",
      'img_url':
      'https://images.pexels.com/photos/67559/kuala-lumpur-petronas-twin-towers-malaysia-klcc-67559.jpeg?auto=compress&cs=tinysrgb&w=300',
    },
    {
      'id': 2,
      'browse_type': 'Classical Choice',
      'browse_title': 'The Works',
      'browse_subtitle': 'Apple Music Classical',
      'description':
      'Your destination for the latest and greatest releases from the classical music world.',
      'img_url':
      'https://images.pexels.com/photos/697672/pexels-photo-697672.jpeg?auto=compress&cs=tinysrgb&w=300',
    },
    {
      'id': 3,
      'browse_type': 'New Soundtrack',
      'browse_title': 'Lively Style',
      'browse_subtitle': 'Various Artists',
      'description': 'Select mix of favourites and classical',
      'img_url':
      'https://images.pexels.com/photos/2284165/pexels-photo-2284165.jpeg?auto=compress&cs=tinysrgb&w=300',
    },
    {
      'id': 4,
      'browse_type': 'Hot Playlist',
      'browse_title': 'All-Time Hits',
      'browse_subtitle': 'Apple Music Top Music',
      'description':
      'The latest most anticipated and hot soundtrack is here in Spatial Audio.',
      'img_url':
      'https://images.pexels.com/photos/1047442/pexels-photo-1047442.jpeg?auto=compress&cs=tinysrgb&w=300',
    },
  ];

  static List<Map<String, dynamic>> dontMissItems = [
    {
      'id': 1,
      'title': 'New Music Daily',
      'subtitle': 'Apple Music',
      'img_url': 'https://images.pexels.com/photos/4061541/pexels-photo-4061541.jpeg?auto=compress&cs=tinysrgb&w=300',
    },
    {
      'id': 2,
      'title': "Today's Hits",
      'subtitle': 'Apple Music Hits',
      'img_url': 'https://images.pexels.com/photos/2341112/pexels-photo-2341112.jpeg?auto=compress&cs=tinysrgb&w=300',
    },
    {
      'id': 3,
      'title': 'Hits in Spatial Audio',
      'subtitle': 'Apple Music hits',
      'img_url': 'https://media.istockphoto.com/id/141256347/photo/mobile-phone-with-speech-bubbles.jpg?b=1&s=612x612&w=0&k=20&c=QRVYJSgHinhBm8oJQEQH_pjuJdfgyVBlBzBTxRGrQb0=',
    },
    {
      'id': 4,
      'title': 'Song Bird',
      'subtitle': 'Apple Music',
      'img_url': 'https://images.pexels.com/photos/16024508/pexels-photo-16024508/free-photo-of-flying-bald-eagle.jpeg?auto=compress&cs=tinysrgb&w=300',
    }
  ];
}
