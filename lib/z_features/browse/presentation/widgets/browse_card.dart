import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class BrowseCard extends StatelessWidget {
  final String browseType;
  final String browseTitle;
  final String browseSubTitle;
  final String description;
  final String imgUrl;

  const BrowseCard({
    super.key,
    required this.browseType,
    required this.browseTitle,
    required this.browseSubTitle,
    required this.description,
    required this.imgUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            browseType.toUpperCase(),
            style: const TextStyle(fontSize: 10, color: Colors.black54),
          ),
          Text(
            browseTitle,
            style: const TextStyle(fontSize: 16, color: Colors.black),
          ),
          Text(
            browseSubTitle,
            style: const TextStyle(
              fontSize: 16,
              color: Colors.black54,
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              height: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: CachedNetworkImageProvider(imgUrl)),
              ),
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        description,
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
