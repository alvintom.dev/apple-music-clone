import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class DontMissCard extends StatelessWidget {
  final double cardWidth;
  final String title;
  final String subtitle;
  final String imgUrl;

  const DontMissCard({
    super.key,
    required this.cardWidth,
    required this.title,
    required this.subtitle,
    required this.imgUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 15),
      width: cardWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CachedNetworkImage(
              imageUrl: imgUrl,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(height: 5),
          Text(
            title,
            style: const TextStyle(fontSize: 14, color: Colors.black),
          ),
          Text(
            subtitle,
            style: const TextStyle(
                fontSize: 14,
                color: Colors.black54,
                fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }
}
