import 'package:apple_music_clone/z_features/browse/data/datasources/sample_data.dart';
import 'package:apple_music_clone/z_features/browse/presentation/widgets/browse_card.dart';
import 'package:apple_music_clone/z_features/browse/presentation/widgets/dont_miss_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrowseScreen extends StatelessWidget {
  const BrowseScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidthMinusPadding =
        MediaQuery.of(context).size.width - (15 * 2);

    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScroll) {
        return <Widget>[
          SliverAppBar.medium(
            pinned: true,
            surfaceTintColor: Colors.transparent,
            shadowColor: Colors.black45,
            title: const Text('Browse'),
            excludeHeaderSemantics: true,
            actions: const [
              IconButton(
                onPressed: null,
                icon: Icon(Icons.more_vert, color: Colors.red),
              )
            ],
          ),
        ];
      },
      body: _getBody(screenWidthMinusPadding),
    );
  }

  Widget _getBody(double screenWidthMinusPadding) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Divider(
              color: Colors.black26,
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 260,
              child: PageView.builder(
                  itemCount: SampleData.browseItems.length,
                  itemBuilder: (context, index) {
                    final page = SampleData.browseItems[index];
                    return BrowseCard(
                      browseType: page['browse_type'],
                      browseTitle: page['browse_title'],
                      browseSubTitle: page['browse_subtitle'],
                      description: page['description'],
                      imgUrl: page['img_url'],
                    );
                  }),
            ),

            const SizedBox(height: 30),

            //section: don't miss this
            const Row(
              children: [
                Text(
                  "Don't Miss This",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 5),
                Icon(
                  CupertinoIcons.right_chevron,
                  color: Colors.black,
                  size: 18,
                )
              ],
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: (screenWidthMinusPadding / 2) + 35,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: SampleData.dontMissItems.length,
                itemBuilder: (context, index) {
                  final item = SampleData.dontMissItems[index];
                  return DontMissCard(
                    cardWidth: (screenWidthMinusPadding / 2) - 10,
                    title: item['title'],
                    subtitle: item['subtitle'],
                    imgUrl: item['img_url'],
                  );
                },
              ),
            ),

            const SizedBox(height: 30),

            //section: k-pop
            const Row(
              children: [
                Text(
                  "New in K-Pop",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 5),
                Icon(
                  CupertinoIcons.right_chevron,
                  color: Colors.black,
                  size: 18,
                )
              ],
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: (screenWidthMinusPadding / 2) + 50,
              child: ListView.builder(
                reverse: true,
                scrollDirection: Axis.horizontal,
                itemCount: SampleData.dontMissItems.length,
                itemBuilder: (context, index) {
                  final item = SampleData.dontMissItems[index];
                  return DontMissCard(
                    cardWidth: (screenWidthMinusPadding / 2) - 10,
                    title: item['title'],
                    subtitle: item['subtitle'],
                    imgUrl: item['img_url'],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
