import 'package:apple_music_clone/z_features/search/data/datasources/remote/search_api_service.dart';
import 'package:apple_music_clone/z_features/search/data/repositories/search_repository_impl.dart';
import 'package:apple_music_clone/z_features/search/domain/usecases/get_search_result.dart';
import 'package:apple_music_clone/z_features/search/presentation/blocs/search_bloc.dart';
import 'package:apple_music_clone/z_features/search/presentation/widgets/search_list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ActiveSearchScreen extends StatelessWidget {
  TextEditingController searchInputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SearchBloc>(
      create: (ctx) => SearchBloc(
          GetSearchResultUseCase(SearchRepositoryImpl(SearchApiService()))),
      child: Scaffold(
          appBar: AppBar(
              elevation: 0,
              titleSpacing: 15,
              surfaceTintColor: Colors.white,
              shadowColor: Colors.white,
              leading: Container(),
              leadingWidth: 0,
              title: BlocBuilder<SearchBloc, SearchState>(
                builder: (ctx, state) {
                  return CupertinoTextField(
                    controller: searchInputController,
                    autofocus: true,
                    onEditingComplete: () {
                      ctx.read<SearchBloc>().add(
                          InitSearch(searchText: searchInputController.text));
                    },
                    placeholder: 'Artists, Songs, Lyrics, and more',
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey.withOpacity(0.2),
                    ),
                    prefix: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(
                        CupertinoIcons.arrow_left,
                        color: Colors.red,
                        size: 20,
                      ),
                    ),
                    suffix: IconButton(
                      onPressed: () {
                        searchInputController.clear();
                      },
                      icon: const Icon(
                        CupertinoIcons.clear,
                        color: Colors.grey,
                        size: 20,
                      ),
                    ),
                    suffixMode: OverlayVisibilityMode.editing,
                  );
                },
              )),
          body: DefaultTabController(
            length: 6,
            child: Column(
              children: [
                const TabBar(
                  labelColor: Colors.red,
                  unselectedLabelColor: Colors.grey,
                  isScrollable: true,
                  indicatorColor: Colors.red,
                  indicatorSize: TabBarIndicatorSize.tab,
                  tabs: [
                    Tab(text: 'MUSICS'),
                    Tab(text: 'ALBUMS'),
                    Tab(text: 'ARTISTS'),
                    Tab(text: 'PLAYLISTS'),
                    Tab(text: 'RADIOS'),
                    Tab(text: 'MUSIC VIDEOS'),
                  ],
                ),
                Expanded(
                  child: BlocBuilder<SearchBloc, SearchState>(
                    builder: (_, state) {
                      if (state is SearchLoadingState) {
                        return const Center(
                          child: CupertinoActivityIndicator(),
                        );
                      }
                      if (state is SearchErrorState) {
                        return const Center(
                          child: Text('Fail to retrieve data'),
                        );
                      }
                      if (state is SearchResultState) {
                        return TabBarView(children: [
                          ListView.separated(
                            itemCount: state.searchResult.musics.length,
                            itemBuilder: (context, index) {
                              final musics = state.searchResult.musics[index];
                              return SearchListItem(
                                title: musics.name,
                                artists: 'Song - ${musics.artistName}',
                                imgUrl: musics.img,
                              );
                            },
                            separatorBuilder: (_, i) => const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Divider(color: Colors.black26),
                            ),
                          ),
                          ListView.separated(
                            itemCount: state.searchResult.albums.length,
                            itemBuilder: (context, index) {
                              final albums = state.searchResult.albums[index];
                              return SearchListItem(
                                title: albums.name,
                                artists: 'Album - ${albums.artistName}',
                                imgUrl: albums.img,
                              );
                            },
                            separatorBuilder: (_, i) => const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Divider(color: Colors.black26),
                            ),
                          ),
                          ListView.separated(
                            itemCount: state.searchResult.artists.length,
                            itemBuilder: (context, index) {
                              final artists = state.searchResult.artists[index];
                              return SearchListItem(
                                title: artists.name,
                                artists: 'Followers - ${artists.followerCount}',
                                imgUrl: artists.img,
                              );
                            },
                            separatorBuilder: (_, i) => const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Divider(color: Colors.black26),
                            ),
                          ),
                          const Center(child: Text('Playlist coming soon!')),
                          const Center(child: Text('Radios coming soon!')),
                          const Center(
                              child: Text('Music videos coming soon!')),
                        ]);
                      }

                      return const SizedBox();
                    },
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
