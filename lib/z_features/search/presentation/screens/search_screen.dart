import 'package:apple_music_clone/z_features/search/presentation/screens/active_search_screen.dart';
import 'package:apple_music_clone/z_features/search/presentation/widgets/search_category_card.dart';
import 'package:flutter/material.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScroll) {
        return <Widget>[
          SliverAppBar.medium(
            pinned: true,
            surfaceTintColor: Colors.transparent,
            shadowColor: Colors.black45,
            title: const Text('Search'),
            excludeHeaderSemantics: true,
            actions: const [
              IconButton(
                onPressed: null,
                icon: Icon(Icons.more_vert, color: Colors.red),
              )
            ],
          ),
          SliverPersistentHeader(
              pinned: true,
              delegate: _PinnedHeaderDelegate(
                  minHeight: 60,
                  maxHeight: 60,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ActiveSearchScreen(),
                        ),
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 7),
                      color: Colors.white,
                      child: Center(
                          child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 13),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.withOpacity(0.2),
                        ),
                        child: const Text(
                          'Artists, Songs, Lyrics and more',
                          style: TextStyle(fontSize: 14, color: Colors.grey),
                        ),
                      )),
                    ),
                  )))
        ];
      },
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: CustomScrollView(
          slivers: [
            SliverGrid(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisExtent: 100,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
              ),
              delegate: SliverChildListDelegate(
                [
                  const SearchCategoryCard(
                    title: 'Malaysian Music',
                    imgUrl:
                        'https://images.pexels.com/photos/3011004/pexels-photo-3011004.jpeg?auto=compress&cs=tinysrgb&w=300',
                    tintColor: Colors.deepOrange,
                  ),
                  const SearchCategoryCard(
                    title: 'K-Pop',
                    imgUrl:
                        'https://images.pexels.com/photos/818593/pexels-photo-818593.jpeg?auto=compress&cs=tinysrgb&w=300',
                    tintColor: Colors.blueAccent,
                  ),
                  const SearchCategoryCard(
                    title: 'Indonesian Music',
                    imgUrl:
                        'https://images.pexels.com/photos/1009949/pexels-photo-1009949.jpeg?auto=compress&cs=tinysrgb&w=300',
                    tintColor: Colors.purpleAccent,
                  ),
                  const SearchCategoryCard(
                    title: 'DJ Mix',
                    imgUrl:
                        'https://images.pexels.com/photos/2381596/pexels-photo-2381596.jpeg?auto=compress&cs=tinysrgb&w=300',
                    tintColor: Colors.deepPurple,
                  ),
                  const SearchCategoryCard(
                    title: 'Rock',
                    imgUrl:
                        'https://images.pexels.com/photos/164693/pexels-photo-164693.jpeg?auto=compress&cs=tinysrgb&w=300',
                    tintColor: Colors.green,
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}

class _PinnedHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double minHeight;
  final double maxHeight;
  final Widget child;

  _PinnedHeaderDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => maxHeight;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
