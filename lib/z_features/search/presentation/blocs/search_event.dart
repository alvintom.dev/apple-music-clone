part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class InitSearch extends SearchEvent {
  final String searchText;

  InitSearch({
    required this.searchText,
  });
}
