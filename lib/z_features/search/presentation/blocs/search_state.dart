part of 'search_bloc.dart';

@immutable
abstract class SearchState {}

class SearchInitialState extends SearchState {}

class SearchLoadingState extends SearchState {}

class SearchResultState extends SearchState {
  SearchResultModel searchResult;

  SearchResultState(this.searchResult);
}

class SearchErrorState extends SearchState {}
