import 'dart:async';

import 'package:apple_music_clone/core/resources/http_state.dart';
import 'package:apple_music_clone/z_features/search/data/models/search_result_model.dart';
import 'package:apple_music_clone/z_features/search/domain/usecases/get_search_result.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'search_event.dart';

part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final GetSearchResultUseCase _getSearchResultUseCase;

  SearchBloc(this._getSearchResultUseCase) : super(SearchInitialState()) {
    on<InitSearch>(initSearch);
  }

  FutureOr<void> initSearch(
    InitSearch event,
    Emitter<SearchState> emit,
  ) async {
    emit(SearchLoadingState());

    final httpState = await _getSearchResultUseCase(event.searchText);

    if (httpState is HttpSuccess && httpState.data != null) {
      emit(SearchResultState(httpState.data!));
    }

    if (httpState is HttpFailed) {
      emit(SearchErrorState());
    }
  }
}
