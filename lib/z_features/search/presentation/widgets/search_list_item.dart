import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SearchListItem extends StatelessWidget {
  final String title;
  final String artists;
  final String imgUrl;

  const SearchListItem({
    super.key,
    required this.title,
    required this.artists,
    required this.imgUrl,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: AspectRatio(
        aspectRatio: 1,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: CachedNetworkImage(
            imageUrl: imgUrl,
            fit: BoxFit.cover,
          ),
        ),
      ),
      title: Text(title),
      subtitle: Text(
        artists,
        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
      ),
    );
  }
}
