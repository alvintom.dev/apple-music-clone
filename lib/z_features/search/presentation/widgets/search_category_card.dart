import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SearchCategoryCard extends StatelessWidget {
  final String title;
  final String imgUrl;
  final Color tintColor;

  const SearchCategoryCard(
      {super.key, required this.title, required this.imgUrl, required this.tintColor});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover, image: CachedNetworkImageProvider(imgUrl)),
        ),
        child: Stack(
          children: [
            Positioned.fill(
                child: Container(
              color: tintColor.withOpacity(0.4),
            )),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  title,
                  style: const TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
