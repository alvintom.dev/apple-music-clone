import 'package:apple_music_clone/core/resources/http_state.dart';
import 'package:apple_music_clone/core/usecases/usecase.dart';
import 'package:apple_music_clone/z_features/search/data/models/search_result_model.dart';
import 'package:apple_music_clone/z_features/search/domain/repositories/search_repository.dart';

class GetSearchResultUseCase
    implements UseCase<HttpState<SearchResultModel>, void> {
  final SearchRepository _searchRepository;

  GetSearchResultUseCase(this._searchRepository);

  @override
  Future<HttpState<SearchResultModel>> call(String query) {
    return _searchRepository.getSearchResult(query);
  }
}
