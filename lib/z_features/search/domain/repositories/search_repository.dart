import 'package:apple_music_clone/core/resources/http_state.dart';
import 'package:apple_music_clone/z_features/search/data/models/search_result_model.dart';

abstract class SearchRepository {
  Future<HttpState<SearchResultModel>> getSearchResult(String query);
}