import 'package:apple_music_clone/data/models/album_model.dart';
import 'package:apple_music_clone/data/models/artist_model.dart';
import 'package:apple_music_clone/data/models/music_model.dart';

class SearchResultModel {
  List<AlbumModel> albums = [];
  List<MusicModel> musics = [];
  List<ArtistModel> artists = [];

  SearchResultModel(this.albums, this.musics, this.artists);

  SearchResultModel.fromJson(Map<String, dynamic> map) {
    albums = <AlbumModel>[];
    musics = <MusicModel>[];
    artists = <ArtistModel>[];

    map['albums']['items'].forEach((album) {
      String img;
      try {
        img = album['images'][0]['url'];
      } catch (e) {
        img =
            'https://images.pexels.com/photos/938580/pexels-photo-938580.jpeg?auto=compress&cs=tinysrgb&w=1600';
      }

      albums.add(AlbumModel(
        id: album['id'],
        artistName: album['artists'][0]['name'],
        name: album['name'],
        img: img,
      ));
    });

    map['tracks']['items'].forEach((music) {
      String img;
      try {
        img = music['album']['images'][0]['url'];
      } catch (e) {
        img =
            'https://images.pexels.com/photos/938580/pexels-photo-938580.jpeg?auto=compress&cs=tinysrgb&w=1600';
      }

      musics.add(MusicModel(
        id: music['id'],
        artistName: music['artists'][0]['name'],
        name: music['name'],
        img: img,
      ));
    });

    map['artists']['items'].forEach((artist) {
      String img;
      try {
        img = artist['images'][0]['url'];
      } catch (e) {
        img =
            'https://images.pexels.com/photos/938580/pexels-photo-938580.jpeg?auto=compress&cs=tinysrgb&w=1600';
      }

      artists.add(ArtistModel(
          id: artist['id'],
          name: artist['name'],
          followerCount: artist['followers']['total'],
          img: img));
    });
  }
}
