import 'dart:io';
import 'package:apple_music_clone/core/resources/http_state.dart';
import 'package:apple_music_clone/z_features/search/data/datasources/remote/search_api_service.dart';
import 'package:apple_music_clone/z_features/search/data/models/search_result_model.dart';
import 'package:apple_music_clone/z_features/search/domain/repositories/search_repository.dart';
import 'package:dio/dio.dart';

class SearchRepositoryImpl implements SearchRepository {
  final SearchApiService _searchApiService;

  SearchRepositoryImpl(this._searchApiService);

  @override
  Future<HttpState<SearchResultModel>> getSearchResult(String query) async {
    try {
      final res = await _searchApiService.getSearchResult(query);

      if (res.statusCode == HttpStatus.ok) {
        return HttpSuccess(res.data!);
      }

      return HttpFailed(DioException(
        requestOptions: res.requestOptions,
      ));
    } on DioException catch (e) {
      return HttpFailed(e);
    }
  }
}
