import 'dart:io';
import 'package:apple_music_clone/core/constants/api_constants.dart';
import 'package:apple_music_clone/z_features/search/data/models/search_result_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SearchApiService {
  final _secureStorage = const FlutterSecureStorage();

  Future<Response<SearchResultModel>> getSearchResult(String query) async {
    String token = await _secureStorage.read(key: 'my_token') ?? "";

    Response res = await Dio()
        .get('${musicApiBaseUrl}search?q=$query&type=album%2Ctrack%2Cartist',
            options: Options(headers: {
              HttpHeaders.authorizationHeader: 'Bearer $token',
            }));

    final searchResults = res.data;

    return Response(
      data: SearchResultModel.fromJson(searchResults),
      requestOptions: res.requestOptions,
      statusCode: res.statusCode,
    );
  }
}
