import 'package:apple_music_clone/z_features/library/presentation/widgets/library_list_item.dart';
import 'package:apple_music_clone/z_features/library/presentation/widgets/recently_added_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LibraryScreen extends StatelessWidget {
  const LibraryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidthMinusPadding =
        MediaQuery.of(context).size.width - (15 * 2);

    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScroll) {
        return <Widget>[
          SliverAppBar.medium(
            pinned: true,
            surfaceTintColor: Colors.transparent,
            shadowColor: Colors.black45,
            title: const Text('Library'),
            excludeHeaderSemantics: true,
            actions: const [
              IconButton(
                onPressed: null,
                icon: Icon(Icons.more_vert, color: Colors.red),
              )
            ],
          ),
        ];
      },
      body: _getBody(screenWidthMinusPadding),
    );
  }

  Widget _getBody(screenWidthMinusPadding) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const LibraryListItem(
              icon: CupertinoIcons.music_note_list,
              title: 'Playlist',
            ),
            const LibraryListItem(
              icon: CupertinoIcons.music_mic,
              title: 'Artists',
            ),
            const LibraryListItem(
              icon: CupertinoIcons.music_albums,
              title: 'Albums',
            ),
            const LibraryListItem(
              icon: CupertinoIcons.music_note,
              title: 'Songs',
            ),

            const SizedBox(height: 20),

            //section: recently added
            const Text(
              "Recently Added",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),

            const SizedBox(height: 10),

            SizedBox(
                height: (screenWidthMinusPadding / 2) + 50,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    RecentlyAddedCard(
                      cardWidth: (screenWidthMinusPadding / 2) - 10,
                      title: 'CHARLIE',
                      subtitle: 'Charlie Puth',
                      imgUrl:
                          'https://images.pexels.com/photos/17263151/pexels-photo-17263151/free-photo-of-modern-beatmaker-equipment-on-wooden-table.jpeg?auto=compress&cs=tinysrgb&w=300',
                    ),
                    RecentlyAddedCard(
                      cardWidth: (screenWidthMinusPadding / 2) - 10,
                      title: 'Hold The Girl',
                      subtitle: 'Rina Sawayama',
                      imgUrl:
                          'https://images.pexels.com/photos/9033628/pexels-photo-9033628.jpeg?auto=compress&cs=tinysrgb&w=300',
                    ),
                    RecentlyAddedCard(
                      cardWidth: (screenWidthMinusPadding / 2) - 10,
                      title: 'Kiss The Rain',
                      subtitle: 'Yiruma',
                      imgUrl:
                          'https://images.pexels.com/photos/4544454/pexels-photo-4544454.jpeg?auto=compress&cs=tinysrgb&w=300',
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
