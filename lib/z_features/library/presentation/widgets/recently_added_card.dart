import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class RecentlyAddedCard extends StatelessWidget {
  final double cardWidth;
  final String title;
  final String subtitle;
  final String imgUrl;

  const RecentlyAddedCard({
    super.key,
    required this.cardWidth,
    required this.title,
    required this.subtitle,
    required this.imgUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 15),
      width: cardWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(            height: cardWidth,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.black12),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(imgUrl)),
            ),
          ),
          const SizedBox(height: 5),
          Text(
            title,
            style: const TextStyle(fontSize: 14),
          ),
          Text(
            subtitle,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: const TextStyle(
                fontSize: 14,
                color: Colors.black54,
                fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }
}
