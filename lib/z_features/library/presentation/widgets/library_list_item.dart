import 'package:flutter/material.dart';

class LibraryListItem extends StatelessWidget {
  final IconData icon;
  final String title;

  const LibraryListItem({super.key, required this.icon, required this.title});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: Row(
            children: [
              Icon(
                icon,
                color: Colors.red,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(title, style: const TextStyle(fontSize: 16),)
            ],
          ),
        ),
        const Divider(
          color: Colors.black26,
        )
      ],
    );
  }
}
