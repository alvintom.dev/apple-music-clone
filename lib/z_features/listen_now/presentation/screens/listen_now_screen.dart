import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListenNowScreen extends StatelessWidget {
  const ListenNowScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScroll) {
        return <Widget>[
          SliverAppBar.medium(
            pinned: true,
            surfaceTintColor: Colors.transparent,
            shadowColor: Colors.black45,
            title: const Text('Listen Now'),
            excludeHeaderSemantics: true,
            actions: const [
              IconButton(
                onPressed: null,
                icon: Icon(Icons.more_vert, color: Colors.red),
              )
            ],
          ),
        ];
      },
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return const Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                CupertinoIcons.bell_fill,
                color: Colors.red,
                size: 50,
              ),
              SizedBox(height: 10),
              Text('Coming Soon! Tune in for next update!')
            ],
          ),
        ));
  }
}
