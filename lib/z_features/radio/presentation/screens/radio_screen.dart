import 'package:apple_music_clone/z_features/radio/presentation/widgets/international_broadcaster_card.dart';
import 'package:apple_music_clone/z_features/radio/presentation/widgets/local_broadcaster_card.dart';
import 'package:apple_music_clone/z_features/radio/presentation/widgets/radio_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RadioScreen extends StatelessWidget {
  const RadioScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidthMinusPadding =
        MediaQuery.of(context).size.width - (15 * 2);

    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScroll) {
        return <Widget>[
          SliverAppBar.medium(
            pinned: true,
            surfaceTintColor: Colors.transparent,
            shadowColor: Colors.black45,
            title: const Text('Radio'),
            excludeHeaderSemantics: true,
            actions: const [
              IconButton(
                onPressed: null,
                icon: Icon(Icons.more_vert, color: Colors.red),
              )
            ],
          ),
        ];
      },
      body: _getBody(screenWidthMinusPadding),
    );
  }

  Widget _getBody(double screenWidthMinusPadding) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: SingleChildScrollView(
        child: Column(
          children: [
            const Divider(
              color: Colors.black26,
            ),
            const RadioCard(
              title: 'Apple Music 1',
              subtitle: 'The New music that matters.',
              imgUrl:
                  'https://images.pexels.com/photos/848573/pexels-photo-848573.jpeg?auto=compress&cs=tinysrgb&w=300',
              mainDescription: 'Sun Kissed',
              description:
                  'The soundtrack to show those hazy moments after a day in the sun.',
              boxColor: Colors.brown,
            ),
            const RadioCard(
              title: 'Apple Music Hits',
              subtitle: 'Song you know and love.',
              imgUrl:
                  'https://images.pexels.com/photos/7083673/pexels-photo-7083673.jpeg?auto=compress&cs=tinysrgb&w=300',
              mainDescription: 'Easy Hits Radio With Sabi',
              description:
                  'Unwind to timeless hits from top artists around the world.',
              boxColor: Colors.grey,
            ),
            const RadioCard(
              title: 'Apple Music Country',
              subtitle: 'Where it sounds like home.',
              imgUrl:
                  'https://images.pexels.com/photos/2228577/pexels-photo-2228577.jpeg?auto=compress&cs=tinysrgb&w=300',
              mainDescription: 'Country Radio',
              description:
                  'Lost and found country music that makes you feel nostalgic.',
              boxColor: Colors.blueGrey,
            ),

            const SizedBox(height: 20),

            //section: local broadcaster
            const Row(
              children: [
                Text(
                  "Local Broadcaster",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 5),
                Icon(
                  CupertinoIcons.right_chevron,
                  color: Colors.black,
                  size: 18,
                )
              ],
            ),

            const SizedBox(height: 10),

            SizedBox(
                height: (screenWidthMinusPadding / 2) + 50,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    LocalBroadcasterCard(
                      cardWidth: (screenWidthMinusPadding / 2) - 10,
                      title: 'Era FM',
                      subtitle: 'Malaysia Oldest Radio',
                      imgUrl:
                          'https://static.wikia.nocookie.net/logopedia/images/6/6d/Era_2018.png/revision/latest/scale-to-width-down/250?cb=20190529043859',
                    ),
                    LocalBroadcasterCard(
                      cardWidth: (screenWidthMinusPadding / 2) - 10,
                      title: 'Mix FM',
                      subtitle: 'Listen to various music genre.',
                      imgUrl:
                          'https://static.wikia.nocookie.net/logopedia/images/e/ed/Mix2017.png/revision/latest/scale-to-width-down/200?cb=20190810135004',
                    ),
                    LocalBroadcasterCard(
                      cardWidth: (screenWidthMinusPadding / 2) - 10,
                      title: 'Sinar FM',
                      subtitle: 'New radio station.',
                      imgUrl:
                          'https://static.wikia.nocookie.net/logopedia/images/9/92/Sinar_Radio.png/revision/latest?cb=20210802041806',
                    ),
                  ],
                )),

            const SizedBox(height: 20),

            //section: international broadcaster
            const Row(
              children: [
                Text(
                  "International Broadcaster",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 5),
                Icon(
                  CupertinoIcons.right_chevron,
                  color: Colors.black,
                  size: 18,
                )
              ],
            ),

            const SizedBox(height: 10),

            const InternationalBroadcasterCard(
              title: 'BBC Asian Network',
              origin: 'Tunein',
              char: 'A',
              color: Colors.purple,
            ),
            const InternationalBroadcasterCard(
              title: 'BBC Radio 1',
              origin: 'Tunein',
              char: '1',
              color: Colors.white,
            ),
            const InternationalBroadcasterCard(
              title: 'BBC Radio 2',
              origin: 'Tunein',
              char: '2',
              color: Colors.orange,
            ),
          ],
        ),
      ),
    );
  }
}
