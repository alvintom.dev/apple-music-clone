import 'package:flutter/material.dart';

class InternationalBroadcasterCard extends StatelessWidget {
  final String title;
  final String origin;
  final String char;
  final Color color;

  const InternationalBroadcasterCard({
    super.key,
    required this.title,
    required this.origin,
    required this.char,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            width: 120,
            height: 120,
            color: Colors.black87,
            child: Center(
              child: Row(
                children: [
                  Expanded(child: _getBBCRadio()),
                  _getCircleLetter(char, color)
                ],
              ),
            ),
          ),
          const SizedBox(width: 15),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: const TextStyle(fontSize: 16, color: Colors.black),
                ),
                Text(
                  'From $origin',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: const TextStyle(
                    fontSize: 10,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _getBBCRadio() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: 12,
                height: 12,
                color: Colors.white,
                child: const Center(
                    child: Text(
                  'B',
                  style: TextStyle(fontSize: 8),
                ))),
            const SizedBox(width: 3),
            Container(
                width: 12,
                height: 12,
                color: Colors.white,
                child: const Center(
                    child: Text(
                  'B',
                  style: TextStyle(fontSize: 8),
                ))),
            const SizedBox(width: 3),
            Container(
                width: 12,
                height: 12,
                color: Colors.white,
                child: const Center(
                    child: Text(
                  'C',
                  style: TextStyle(fontSize: 8),
                ))),
          ],
        ),
        const Text(
          'RADIO',
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }

  Widget _getCircleLetter(String text, Color color) {
    return Container(
      width: 40,
      height: 40,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(50), color: color),
      child: Center(
        child: Text(
          text,
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
