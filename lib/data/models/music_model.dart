import 'package:apple_music_clone/domain/entities/music_entity.dart';

class MusicModel extends MusicEntity {
  MusicModel({
    required super.id,
    required super.artistName,
    required super.name,
    required super.img,
  });

  factory MusicModel.fromJson(Map<String, dynamic> map) {
    return MusicModel(
        id: map['id'] ?? "",
        artistName: map[''],
        name: map['name'] ?? "",
        img: map['']);
  }
}
