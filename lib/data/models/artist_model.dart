import 'package:apple_music_clone/domain/entities/artist_entity.dart';

class ArtistModel extends ArtistEntity {
  ArtistModel({
    required super.id,
    required super.name,
    required super.followerCount,
    required super.img,
  });

  factory ArtistModel.fromJson(Map<String, dynamic> map) {
    return ArtistModel(
        id: map['id'] ?? "",
        name: map[''],
        followerCount: map['name'] ?? "",
        img: map['']);
  }
}
