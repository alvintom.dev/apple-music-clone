import 'package:apple_music_clone/domain/entities/album_entity.dart';

class AlbumModel extends AlbumEntity {
  AlbumModel({
    required super.id,
    required super.artistName,
    required super.name,
    required super.img,
  });

  factory AlbumModel.fromJson(Map<String, dynamic> map) {
    return AlbumModel(
        id: map['id'] ?? "",
        artistName: map[''],
        name: map['name'] ?? "",
        img: map['']);
  }
}
