part of 'main_bloc.dart';

@immutable
abstract class MainEvent {}

class RetrieveTokenEvent extends MainEvent {}

class TapBottomNavEvent extends MainEvent {
  final MainScreenType mainScreenType;

  TapBottomNavEvent({
    required this.mainScreenType,
  });
}
