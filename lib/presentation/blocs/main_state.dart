part of 'main_bloc.dart';

@immutable
abstract class MainState {}

class RetrievingTokenState extends MainState {}

class SelectedTabState extends MainState {
  final MainScreenType mainScreenType;

  SelectedTabState({
    required this.mainScreenType,
  });
}
