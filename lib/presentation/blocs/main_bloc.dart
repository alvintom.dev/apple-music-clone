import 'dart:async';
import 'dart:io';
import 'package:apple_music_clone/core/constants/api_constants.dart';
import 'package:apple_music_clone/core/resources/token_api_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

part 'main_event.dart';

part 'main_state.dart';

enum MainScreenType { listenNow, browse, radio, library, search }

class MainBloc extends Bloc<MainEvent, MainState> {
  final _tokenApiService = TokenApiService();
  final _secureStorage = const FlutterSecureStorage();

  MainBloc() : super(RetrievingTokenState()) {
    _fetchToken();

    on<TapBottomNavEvent>(tapBottomNavEvent);
  }

  //I directly put the token method here just to expedite this up.
  //Token or authentication method should be handle in authentication use case and put at parent folder 'authentication'
  //token also better save inside secure storage
  //auto refresh token also have not been handle yet, it only refresh upon starting the app
  Future<void> _fetchToken() async {

    try {
      final response = await _tokenApiService.getToken(clientSecret);

      if (response.statusCode == HttpStatus.ok && response.data != null) {
        // Write value
        _secureStorage.write(
            key: 'my_token', value: response.data!['access_token']);
      }
    } catch (e) {
      //just continue
    }

    emit(SelectedTabState(mainScreenType: MainScreenType.radio));
  }

  FutureOr<void> tapBottomNavEvent(
      TapBottomNavEvent event, Emitter<MainState> emit) {
    emit(SelectedTabState(mainScreenType: event.mainScreenType));
  }
}
