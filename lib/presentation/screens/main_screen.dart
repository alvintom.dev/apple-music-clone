import 'package:apple_music_clone/z_features/browse/presentation/screens/browse_screen.dart';
import 'package:apple_music_clone/z_features/library/presentation/screens/library_screen.dart';
import 'package:apple_music_clone/z_features/listen_now/presentation/screens/listen_now_screen.dart';
import 'package:apple_music_clone/z_features/radio/presentation/screens/radio_screen.dart';
import 'package:apple_music_clone/z_features/search/presentation/screens/search_screen.dart';
import 'package:apple_music_clone/presentation/blocs/main_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(

        builder: (context, state) {
      return state is RetrievingTokenState
          ? const Scaffold(body: Center(child: CupertinoActivityIndicator()))
          : Scaffold(
              body: _getBody(state),
              bottomNavigationBar: _buildBottomNavigationBar(context, state),
            );
    });
  }

  Widget _getBody(MainState state) {
    if (state is SelectedTabState) {
      switch (state.mainScreenType) {
        case MainScreenType.listenNow:
          return const ListenNowScreen();
        case MainScreenType.browse:
          return const BrowseScreen();
        case MainScreenType.radio:
          return const RadioScreen();
        case MainScreenType.library:
          return const LibraryScreen();
        case MainScreenType.search:
          return const SearchScreen();
      }
    }

    return Container();
  }

  Widget _buildBottomNavigationBar(BuildContext context, MainState state) {
    int currentIndex = 0;

    if (state is SelectedTabState) {
      currentIndex = state.mainScreenType.index;
    }

    return BottomNavigationBar(
      currentIndex: currentIndex,
      type: BottomNavigationBarType.fixed,
      elevation: 10,
      selectedIconTheme: const IconThemeData(
        size: 22,
      ),
      unselectedIconTheme: const IconThemeData(
        size: 22,
      ),
      selectedItemColor: Colors.red,
      unselectedItemColor: Colors.black26,
      selectedFontSize: 10,
      unselectedFontSize: 10,
      onTap: (index) {
        MainScreenType mainScreenType;
        switch (index) {
          case 0:
            mainScreenType = MainScreenType.listenNow;
            break;
          case 1:
            mainScreenType = MainScreenType.browse;
            break;
          case 2:
            mainScreenType = MainScreenType.radio;
            break;
          case 3:
            mainScreenType = MainScreenType.library;
            break;
          case 4:
            mainScreenType = MainScreenType.search;
            break;
          default:
            mainScreenType = MainScreenType.listenNow;
            break;
        }
        context
            .read<MainBloc>()
            .add(TapBottomNavEvent(mainScreenType: mainScreenType));
      },
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(CupertinoIcons.play_circle_fill),
          label: 'Listen Now',
        ),
        BottomNavigationBarItem(
          icon: Icon(CupertinoIcons.rectangle_grid_2x2_fill),
          label: 'Browse',
        ),
        BottomNavigationBarItem(
          icon: Icon(CupertinoIcons.dot_radiowaves_left_right),
          label: 'Radio',
        ),
        BottomNavigationBarItem(
          icon: Icon(CupertinoIcons.music_albums_fill),
          label: 'Library',
        ),
        BottomNavigationBarItem(
          icon: Icon(CupertinoIcons.search),
          label: 'Search',
        ),
      ],
    );
  }
}
