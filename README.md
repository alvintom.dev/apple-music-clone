# apple_music_clone

This is a basic clone of Apple Music, but it's important to note that not all functions are
currently operational, such as the ability to play music.

## Intro

This is the very basic of Apple Music Clone app using flutter.

However, with limited time, there are only fews feature I am able to complete.

Also, within this project, I tried to stick with clear architecture principle where I mahae my folder and data in three different layers (domain, data, presentation).

## checklist overview

*   [x] **4 basic** screens (Browse, Radio, Library, Search)
*   [x] **Search** musics, albums and song features
*   [x] **Dio** http client
*   [x] **Bloc** state management
*   [x] **Sliver** view
*   [x] **Loading** indicator
*   [x] **Model** usage
*   [x] **Clean Architecture**

## extra

*   [x] **Authentication**
*   [x] **Secured Storage**
*   [x] **Network Image Cached**

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/alvintom.dev/apple-music-clone.git
```

Install dependencies

```
flutter pub get
```

## Here is some screenshots

![Diagram](doc/browse.jpg)
![Diagram](doc/radio.jpg)
![Diagram](doc/library.jpg)
![Diagram](doc/search.jpg)
![Diagram](doc/music.jpg)
![Diagram](doc/artist.jpg)

### Testing

No unit test implemented. Maybe in the future i will add it for a better code show case.

## Contribution

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Conclusion

There are some difficulties when cloning Apple Music app (For Me Currently) because I don't have enough references for the UI parts. Because of that, some UI parts and experience may not feel as close as it could to Apple Music App.

Thank You.
